 ( 143557 ) 91-07-27 13:09:00 Pekka M_kinen erased 
    Expiration date: 92-07-26 00:00:00  Characters:  24165   Marks: 1 
Receiver: Pitkät puheenvuorot (long entries), received: 91-07-27 13:09:00
Subject: Timo Kiravuon NEWS-ohjeet (otaxista)
---------------------------------------------


Newsgroups: otax.ekakerta,atk.tietoliikenne
Subject: News-etiketti osa 1; Johdanto
Message-ID: <KIRAVUO.91Jul26181905@hila.hut.fi>
Organization: Helsinki University of Technology, Computing Center


	Miksi olutta on parempi juoda pullosta kuin lasista?
	Koska etiketistä on aina hyvä pitää kiinni.


Johdanto

Tämä on ohje. Ei sääntö. Eikä ohjesääntö.  Kirjoitin tämän, jotta verkkoa
käyttävät ihmiset tulisivat paremmin toimeen keskenään ilman että heidän
täytyisi oppia kaikki kantapään kautta.

Koepostaus 26.7 ryhmiin otax.ekakerta ja atk.tietoliikenne,
keskustelu jatkokäytöstä ryhmässä otax.ekakerta, todennäköinen
jatkokäyttö ryhmässä aloita_tasta. Kommentit Timo Kiravuolle,
kiravuo@hut.fi. 

Aloittaminen

Älä aloita newsseihin postaamista (kirjoittamista) ennen kuin olet
seurannut systeemin toimintaa tarpeeksi pitkään. Paikallisten ryhmien
(Otax) suhteen se tarkoittaa ehkä kuukautta, kansainvälisten suhteen puolta
vuotta. Postatessasi ensimmäistä kertaa tee se johonkin testiryhmään, kuten
otax.test tai sfnet.test. Aloittelijoille-ryhmät eivät ole testaamista
varten, vaan aloittelijoiden kysymyksiä varten.

-----------------------------------------------------------------------

Todennäköisesti et kirjota newsseihin vain sormiesi lämmittelemiseksi vaan
haluat välittää jonkin viestin muille ihmisille. Tässä yritän kertoa miten
saat viestisi perille paremmin.


Perusteet

Käytä lyhyitä lauseita ja kappaleita. Hyvä kappaleen pituus on viidestä
kymmeneen riviä, lyhyempiäkin saa olla. Erota kappaleet ylimääräisellä
rivinvaihdolla toisistaan.

Kirjoita vasen reuna suorana ja ilman sisennystä, oikea vapaana.  Tasattu
oikea reuna ainoastaan vaikeuttaa lukemista.

Älä kirjoita liian pitkiä rivejä. Sopiva rivinpituus on jossain 65:n ja
71:n välillä. Varmista että rivit päättyvät rivinvaihtoon. (Tämä koskee
etenkin tekstinkäsittelyohjelmiin tottuneita.)  Useimmissa editoreissa on
jonkinlainen wrap-moodi, jolloin rivinvaihto tulee automaattisesti.  Käytä
sitä.


Tekstin tehostaminen

Korostuskeinoja on käytettävissä aika vähän. Päätteenohjauskoodit eivät
selviä newssien läpi. Onneksi. Usein käytetään _alleviivausta_, joskus
näkee *vahvennettua* tekstiä. Toisinaan ihmiset HUUTAVAT. _Alleviivaus_ on
näistä suositeltavin. *Vahvennus* sekoittuu usein säännöllisiin
lausekkeisiin (tiedäthän, *.foo). HUUTAMISTA on erittäin paha lukea ja sitä
ei kannata käyttää paria sanaa pitempään.


Hymiö

Sarkasmi on vaativa tyylilaji. Mikäli et hallitse sitä yrityksesi olla
huvittava tai sarkastinen muuttua kirjoitettuna veriseksi loukkaukseksi.
Kun äänenpaino ja ilmeet eivät ole tukemassa lausetta, sen merkitys muuttuu
helposti.

Tämän vahingon pystyt estämään käyttämällä hymiötä oikeassa paikassa.
Perusmalleja ovat :-), :) ja ;-). Variaatioita on loputtomasti. Lisäksi on
surullinen hymiö :-(. (Jos et näe kuvia, kallista päätäsi vasemmalle 90
astetta.) Älä tunge hymiöitä jokaisen lauseen perään, kevennä vain pahimpia
herjoja.


Lainaus 

Artikkeliin vastatessasi voit lainata edellisen artikkelin tekstiä omassa
artikkelissasi. Lainattu teksti erottuu kirjoittamastasi siitä että rivin
alussa on >-merkki tai se on sisennetty yhden tabulaattorin verran. (Tämän
takia kannattaa kirjoittaa alle 71 merkin rivejä ja olla sisentämättä
vasenta reunaa). Moneen kertaan lainattu teksti erottuu monesta
perättäisestä >-merkistä.

Lainaa vanhaa tekstiä aina mahdollisimman vähän, mieluiten ei ollenkaan.
Lukijasi on saattanut lukea edellisen artikkelin juuri ennen omaasi tai
sitten viikkoa aikaisemmin.  Artikkelisi tulisi pystyä kertomaan molemmissa
tapauksissa mistä on kyse. Yleensä kohtuullinen lainaus on edellisen
kirjoittajan nimi ja muutama ydinrivi jotka paljastavat mistä on kyse. Tai
kirjota omin sanoin lyhennelmän artikkelista johon vastaat.

Kokonaisen artikkelin lainaaminen riviäkään poistamatta on äärimmäisen
epäkohteliasta, älä tee niin. Useimmat järjestelmät rajoittavat lainatun
tekstin määrän puoleen artikkelin pituudesta. Tämä siksi että jos sinulla
ei ole enempää omaa sanomista, niin olisi parempi että pitäisit turpasi
tukossa.

Pitkät artikkelit joissa käydään jonkun toisen kirjoittajan juttua läpi
rivi riviltä ja vastataan jokaiseen riviin erikseen ovat varsin tylsiä.
Eikä niitä yleensä juuri kukaan luekaan.


Käytettävä kieli

Otaxin ja sfnetin ryhmissä käytetään pääsääntöisesti suomea.
Ruotsinkielisissä ryhmissä ruotsia. Kansainvälisissä ryhmissä
pääsääntöisesti englantia.

Älä käytä suomea tai ruotsia kansainvälisissä ryhmissä.

Vältä ammattisanastoa yleisessä keskustelussa. Jos keskustelu edellyttää
erikoistermejä, käytä niitä. Muista että usein ryhmää lukee myös joukko
alaa tuntemattomia mutta kiinnostuneita ihmisiä, jotka arvostaisivat
termien selitystä.


ÅÖÄåöä

Tällä hetkellä käytetään 7-bittisiä skandinaavisia merkkejä. Siis niitä
jotka toisinaan näkyvät hakasulkeina. News-järjestelmä ei tue muita.


Signature 

Signature eli allekirjoitus on se muutaman rivin mittainen pätkä artikkelin
lopussa, jossa on kirjoittajan nimi ja mahdollisesti muita oleellisia
tietoja.

Signature tehdään luomalla kotihakemistoon .signature-niminen tiedosto,
jonka pituus ei ylitä neljää riviä, mukaanlukien tyhjät rivit. Tämä
rajoitus on ehdottoman järkevä, ja siitä on turha urputtaa. Et sinä ole
niin tärkeä että joku haluaisi tietää sinusta sen enempää. :)

Jos .signaturea käyttää, siinä kannattaa olla oma nimi, oman organisaation
ulkopuolella menevissä viesteissä organisaation nimi ja kansainvälisissä
yhteyksissä maa. Enempää ei tarvitse olla.  Sähköpostiosoite näkyy miltei
aina viestin otsikkotiedoista.

Usein on tapana laittaa signatureen jokin mietelause. Se kannattaa vaihtaa
aina silloin tällöin. Muutakin sopivaa voi haluttaessa laitta.  Kunhan
pysyy kohtuudessa. Isot ASCII-kuvat ovat huvittavia _vain_kerran_.

Signaturen rivien maksimipituudeksi on hyvä rajata 79 merkkiä. 80 merkkiä
mahtuu ruudulle, mutta Emacsissa 80. merkki menee seuraavalle riville.

Signaturen lainaaminen followupeissa on turhaa. Kuluttaa vain
tarpeettomasti levytilaa ja tiedonsiirtokapasiteettia.


Loukkaava (offensive) materiaali

Potentiaalisesti loukkaava materiaali (esim. vähemmistövitsit) on tapana
koodata rot-13-koodauksella, joka on tavallinen Caesarin
transpositiosalakirjoitus avaimella 13. Eli kirjaimet A-M korvataan
kirjaimilla N-Z ja N-Z kirjaimilla A-M. Muita kirjaimia tai merkkejä ei
korvata. Useimmissa lukuohjelmissa on Rot-13-koodin purku valmiina jonkin
näppäimen takana. Rn:ssä X ja Gnussissa C-c C-r.

Rot-13-koodausta käyttämällä on vastuu artikkelin mahdollisesti
aiheuttamasta loukkauksesta siirretty lähettäjältä vastaanottajalle
(Yhdysvalloissa Tärkeä Asia). Rot-13-artikkelia ei voi lukea vahingossa,
vaan sen lukemiseksi on tehtävä jotain. Niinpä kukaan ei voi valittaa että
joutui yllättäen loukatuksi.


Tavoite

Kun jutussasi on vain välttämättömin asia lainattuja ja loppu on tiukkaa,
selkeästi esitettyä asiaa, olet oikeilla jäljillä. Muista lyhyet
ydinvirkkeet niin menestyt tässä maailmassa.

--------------------------------------------------------------------

News-järjestelmän olennainen osa on ihmisten välinen keskustelu, tietojen
ja mielipiteiden vaihto. Sen onnistumiseksi sinun on otettava toiset
ihmiset huomioon.


Vastaaminen

Etenkin kansainvälisessä levityksessä käytä newsseissä olleeseen
artikkeliin vastaamiseen ennemmin postia kuin suoraan newsseihin
postaamista. Newssit leviävät uskomattoman laajalle ja kaikki tarpeeton
aiheuttaa ylimääräisiä kustannuksia, vaikka se ei yleensä sinulle näykään.


Followupista flameksi

Edelliseen artikkeliin viittaavaa artikkelia kutsutaan followupiksi.
Followupin tunnistaa mm. siitä että subject-rivi alkaa (yleensä) tekstillä
"Re:". Tulista followupia kutsutaan flameksi. Flamet ovat yleensä
hyökkääviä ja päällekäyviä.

Flame ei yleensä saavuta sitä tarkoitusta mikä sillä näennäisesti on, eli
alkuperäisen artikkelin kirjoittajan valistaminen. Fiksut lukijat
ymmärtävät että kyse on pikemminkin liekittäjän halusta osoittaa omaa
näppäryyttään toisen kustannuksella.

Flamet eivät kuulu rakentavaan keskusteluun.


Uskonsodat

Flamefestejä parhaimmillaan - tai pahimmillaan.  Tyypillisesti tapellaan
joidenkin tiettyjen koneitten tai käyttöjärjestelmien välisistä eroista.
Mac - PC -väittely on ehkä eräs yleisimmistä. Tai UNIX vastaan muut
käyttöjärjestelmät.

Väittelijät yleensä onnistuvat vain osoittamaan oman pikkusieluisuutensa ja
kykenemättömyytensä näkemään suurempia kuvioita väittelyllään. Erityisen
yleisesti unohdetaan että useimmille meistä tietokoneet (useimmat sodat
pyörivät tietokoneiden ympärillä) ovat työkaluja eivätkä mitään
taide-esineitä tai syvällisen palvonnan kohteita. "Kun ainoa työkalu on
vasara, kaikki ongelmat näyttävät nauloilta."

Kun muistat "Asiat tappelevat keskenään, eivät ihmiset", menestyt paremmin.


Sähköpostin lainaaminen newsseissä 

Sähköposti on henkilökohtaista. Sen lainaaminen julkisesti on
epäkohteliasta ja tilanteesta riippuen jopa tekijänoikeuslakien vastaista.
Vastauksen johonkin tekniseen kysymykseen voi yleensä julkaista, mutta
silloinkin kannattaa varmistaa lupa kirjoittajalta. Sen sijaan flame-sodan
aikana vastustajalta saadun henkilökohtaisen viestin julkaiseminen
katsotaan erittäin törkeäksi teoksi.


Älä häiritse muita

Rec.hunting on ryhmä jossa keskustellaan metsästyksestä. Jos olet itse
vannoutunut metsästyksen vastustaja, _älä_ mene julistamaan ideaasi
rec.huntingissa. Metsästäjillä on oikeus keskustella harrastuksestaan
rauhassa. Sillä siisti.

Maailma on täynnä moralisteja, jotka yrittävät pakottaa muut ihmiset
elämään heidän tavallaan. Useimmat heistä eivät ymmärrä moralisoinnillansa
vain lisäävänsä vahinkoa. Jos haluat keskustella jonkin asian
periaatteellisista oikeutuksista, mieti tarkkaan minne keskustelu sopii ja
ota selvää ryhmän tarkoituksesta ensin. Useimmat ihmiset eivät halua
keskustella viittäkymmenettä yhdettätoista kertaa harrastuksensa tai
uskontonsa oikeutuksesta


Väittämät 

Perustele. Esitä referenssejä. Jos esität oma mielipiteesi, sano se
selkeästi, jos kirjoitat jonkin toisen puhumaa, kerro kenen. Vältä termejä
"yleisessä tiedossa" ja "asiantuntijoiden mukaan". Useimmat lukijat ovat
älykkäitä ja ymmärtävät että "yleisessä tiedossa" tarkoittaa samaa kuin
"uskoakseni".

Jos et ole varma asiastasi, sano se. Jos yleensä edustat jotain tahoa, sano
edustatko sitä myös tässä asiassa vai et. Jos olet töissä firmassa joka
valmistaa tai myy medoomeja tee se tiettäväksi jos osallistut keskusteluun
medoomeista. Etenkin jos haukut kilpailijan tuotteita.


Keskeytä, peruuta

Joku on postannut artikkelin joka loukkaa tunteitasi ja saa sinut näkemään
punaista. Aloitat vastauksen, toteat että sinulla ei oikeastaan ole juuri
mitään varsinaista sanomista, mutta tekisi mieli näpäyttää...
_Keskeytä_artikkelisi_. Älä lähetä sitä. Vastaa vaikka postilla.

Sähköpostia ei voi peruuttaa. Newssien postauksen voi. Useimmissa
ohjelmissa se tapahtuu painamalla isoa C-kirjainta (Cancel). Käytä
tarvittaessa.


Kirjoitusvihreet

Niitä sattuu. Mikäli huomaat oman virheesi, ja se ei ole oleellinen, älä
välitä. Jos se on oleellinen, peruuta artikkeli, ja lähetä korjattuna
uudestaan. Jos se on jonkun muun virhe, älä puutu asiaan. Tai lähetä
postia.

Henkilöt jotka lähettävät kansainvälisiin newsseihin huomautuksen jonkun
toisen tekemästä kirjoitusvirheestä, tekevät kaikille lukijoille kerralla
selväksi älykkyysosamääränsä ja kengännumeronsa välisen korrelaation.


Henkilökohtaiset viestit ja muut kyselyt

Jos et saa postia menemään jonnekin, älä postaa kansainvälisiin
newsseihin. Kysy ylläpidolta, soita puhelimella, kirjoita paperikirje jne.
Se tulee huomattavasti halvemmaksi kuin newsseihin postaaminen.  Sama
koskee kysymyksiä jonkin henkilön olinpaikasta tai yhteyksistä tiettyyn
paikkaan. Jos haluat tietää onko Umba-Umba-saarten teknillinen korkeakoulu
verkossa, soita Umba-Umba-saarille ja kysy. Pokkeuksena
soc.net.people-ryhmä, joka on olemassa juuri ihmisten etsimistä varten.


Metakeskustelu

Keskustelua keskustelusta. "Voimmeko puhua tästä asiasta tässä ryhmässä?"
Harvinaisen tylsää. Yritä välttää.


Net.häiriköt, ignore them

Verkko on yhteinen hiekkalaatikko ja siellä pitäisi käyttäytyä sen
mukaisesti. Valitettavasti jotkut haluavat ehdottomasti heittää hiekkaa
toisten silmiin. Kuten jotkut meistä oppivat jo hiekkalaatikolla, paras
tapa päästä heistä eroon on olla huomaamatta heitä.

Eräs tapaus oli henkilö joka halusi välttämättä selittää miten juutalaisten
joukkotuho toisen maailmansodan aikana oli pelkkää sionistipropagandaa.
Muutama henkilö otti yhteyttä hänen käyttämän koneen ylläpitoon ja onnistui
poistamaan hänen lupansa. Seuraus: kyseinen tyyppi hankki luvan toisesta
paikasta ja jatkoi juttujensa levittämistä verkkoon.

Tehokkaampaa olisi ollut laittaa hänen käyttäjätunnuksensa kill-tiedostoon,
jolloin hänen kirjoittamiaan juttuja ei tarvitse lukea. Ongelmana on vielä
ihmisten vastaukset näihin juttuihin, mutta sille ei voi mitään. Mikäli
tällaiset henkilöt häiritsevät liikaa, lopeta newssien lukeminen.

Jos itse käyttäydyt epäkohteliaasti tai lainailet liikaa muiden tekstejä
tai käytät isoa signaturea päädyt hyvin äkkiä useimpien lukijoitten
kill-tiedostoihin. Silloin menetät newsseistä mahdollisesti saatavat
hyödyt, vaikka myöhemmin parantaisit tapasi. Kukaan ei lue juttujasi,
kukaan ei vastaa kysymyksiisi.


--------------------------------------------------------------------


Newsjärjestelmä on jäsennetty ryhmiin. Jokaisella ryhmällä on nimi ja
muutaman rivin mittainen kuvaus, joka määrittää mihin käyttöön ryhmä on
tarkoitettu. Kansainvälisissä ryhmissä tiedot julkaistaan säännöllisesti
ryhmässä news.announce.newusers. Kotimaisten ryhmien lista ja kuvaukset
vastaavasti ryhmässä sfnet.tietoliikenne.ryhmat+listat. Alt-ryhmistä ei
vastaavia tietoja yleensä ole. Kohtuullisen kuvan ryhmän tarkoituksesta saa
myöskin seuraamalla sitä jonkin aikaa.


Valitse ryhmä oikein

Oikean ryhmän valinta on tärkeää. Kannattaa miettiä muutama minuutti mihin
ryhmään asia kuuluu. Väärästä ryhmästä saa vain valituksia.

Mahdollisen töppäyksen vakavuus riippuu ryhmän laajuudesta. Töppäys
otaxissa ei paina paljoakaan. Sfnet-ryhmät leviävät jo laajemmalle.
Isoissa, kansainvälisissä newsseissä ei kannata töppäillä, vaikka muut
ulkomailla niin tekevätkin. Suomalaisilla on toistaiseksi ollut
kohtuullisen hyvä maine, älä pilaa sitä.


Cross-posting 

Jos asia todellakin ja varmasti on niin tärkeä että se kuuluu useaan
ryhmään, älä postaa sitä erikseen joka ryhmään, vaan laitaa ryhmien nimet
Newsgroups: -riville pelkällä pilkulla (ei välilyöntiä) erotettuna.
Useimmat lukuohjelmat osaavat sitten merkitä artikkelin luetuksi muissa
ryhmissä, joten lukijat näkevät sen vain kerran. Ohjaa jatkokeskustelu
yhteen ryhmään Followup-To: -kentällä. Ja käytä tätä vain jos asia on
todellakin riittävän tärkeä.


Keskustelun siirto

Mikäli olet sitä mieltä että keskustelua käydään väärässä ryhmässä, sen voi
siirtää parempaan paikkaan kirjoittamalla artikkelin alkuun "Siirrän
keskustelun. fii.faa.foosta pii.paa.poohon." Kirjoita Newsgroups: -riville
molempien ryhmien nimet ja Followup-To: -riville pii.paa.poo.


Ryhmien luominen

Uutisryhmän perustaminen Suomen news-alueelle on helppoa, yleensä riittää
pyyntö ryhmään sfnet.tietoliikenne.ryhmat+listat tai postia osoitteeseen
ryhmat@funet.fi. Jos pyyntö on suunnilleen järkevä ja kiinnostusta tuntuu
olevan, ryhmä voidaan perustaa ilman sen kummempia ongelmia.

Kansainvälisten ryhmien perustaminen on paljon moninmutkaisempaa. Se
edellyttää puolentoista kuukauden mittaista keskustelu- ja
äänestysprosessia. Tarkemmat ohjeet julkaistaan säännöllisesti ryhmässä
news.announce.newusers.

Alt-ryhmiä voi perustaa kuka tahansa, mutta niistäkin on kohteliasta
keskustella ryhmässä alt.config. Millään taholla ei kuitenkaan ole
velvollisuutta välittää niitä eteenpäin ja niiden levinneisyys onkin
jonkin verran rajoitetumpi kuin varsinaisten ryhmien. 


Uuden ryhmän käynnistyminen

Säännöllisesti kun jokin uusi ryhmä perustetaan, ovat ensimmäiset
postaukset kysymyksiä "Miksi kukaan ei postaa tänne?" ja "Mikä ryhmä tämä
on?" Edellinen kysymys osoittaa että kysyjä on onneton hätiköijä.  Yleensä
uuden ryhmän leviäminen kautta verkon kestää muutaman päivän ja keskustelu
kyllä käynnistyy sitten omalla ajallaan.  Jälkimmäinen että kysyjä ei osaa
itse lukea news.newgroups tai sfnet.tietoliikenne.ryhmat+listat ryhmästä
selvitystä. Lisäksi miltei aina jokin uuden ryhmän luomista aktiivisimmin
ajaneista henkilöistä postaa selvityksen ryhmän tarkoituksesta ja
mahdollisesta historiasta.  Ole kärsivällinen.


Ryhmän piristäminen

Mikäli jossain ryhmässä ei ole keskustelua ja haluaisit sellaista luoda,
älä kysy "onko kaikki tyhmiä vai miks'ei kukaan kirjoita mitään?"
Kohteliaampi tapa herättää keskustelua on kirjoittaa itse jonkinlainen
alustus keskustelulle. Se voi olla kysymys tai essee tai kirjallisuuslista
tai jotain muuta vastaavaa.


Moderointi

Moderointi eli suomeksi toimittaminen tarkoittaa sitä, että ryhmään
postatut artikkelit eivät tule suoraan näkyville, vaan ne kulkeutuvat ensin
moderaattorille, joka sitten voi postata ne tai jättää postaamatta.

Moderointia käytetään joko pitämään keskustelu asiassa tai pitämään
ylimääräiset jutut poissa tiedotusryhmistä. Otaxin tkk.tekola.tiedottaa on
moderoitu ryhmä, jossa on ainoastaan laskentakeskuksen tiedotuksia.
Keskustelua varten on olemassa erilline ryhmä tkk.tekola.

Moderointi on usein oleellinen osa tulenarkojen ryhmien toimintakunnon
säilyttämisessä. Esimerkki hyvästä moderoinnista on sci.military, ryhmä
joka on varattu ainoastaan sotatieteeseen liittyviin keskusteluihin, ei
poliittisiin eikä historiallisiin keskusteluihin. Ilman moderointia
liikenne ryhmässä olisi todennäköisesti kasvanut räjähdysmäisesti
Persianlahden sodan aikana ja ryhmä olisi muuttunut lukukelvottomaksi.

Moderointi vaatii myös moderaattorin eli henkilön joka on valmis istumaan
kuumalla pallilla ja kestämään valituksia. Hänen tehtävänsä on olla
objektiivinen. Useimmat moderaattorit onnistuvat siinä kohtuullisesti.

Itse vertaan moderointia yleensä toimittamiseen. Toiset ovat usein
verranneet sitä sensuuriin. En ymmärrä täysin miksi. Puheenvapaus
news-järjestelmässä ei todellakaan ole mikään oletusarvo. Eivät lehdetkään
julkaise kaikkia yleisönkirjeitä. Ja usein moderoiduille ryhmille on
olemassa myös jokin vaihtoehtoinen ryhmä. Moderointi on olemassa vain
keskustelun tason varmistamiseksi.

On myös paljon ryhmiä jotka toimivat hyvin ilman moderointia. Nämä ovat
yleensä ryhmiä joiden aihe piiri on rajattu eikä erityisen tulenarka.


-------------------------------------------------------------------

Avun pyytäminen

Usein on tarve saada ratkaisu johonkin tekniseen ongelmaan. Tällöin on
ongelma kuvattava riittävän hyvin. Jos kysymys kokonaisuudessaan kuuluu
"miksi en saa yhteyttä Vipuseen?" voisi sen jättää yhtä hyvin kysymättä.
Eri koneiden välille voi luoda yhteyksiä kymmenillä eri tavoilla,
tuollaiseen kysymykseen vastaaminen edellyttäisi kaikkien eri tapojen
keksimistä ja kokeilemista. Kysymykseen pitää aina liittää kuvaus tyyliin
"kun yritin ottaa yhteyttä hilasta vipuseen komennolla 'rlogin vipunen'
kello 18.32 tänään 31.4, kone vastasi 'bus error (core dumped)'" Hyväkään
kysymys ei tietenkään takaa vastausta, mutta se parantaa mahdollisuutta
saada vastaus.

Toisinaan näkee kansainvälisissä ryhmissä kysymyksiä, joiden lopussa lukee
"lähettäkää postia, en seuraa ryhmää." Tämä on varsin epäkohteliasta. Jos
omaan asiaan ei itsellä ole edes tuon vertaa kiinnostusta, niin kuinka voi
kuvitella että joku viitsii vastata?

Sen sijan "lähettäkää postia, postaan yhteenvedon" on erittäin kohteliasta.
Tällöin asiasta kiinnostumaton ei joudu kahlaamaan suuren artikkelimäärän
halki ja asiasta kiinnostuneet lukijat saavat tiiviin informaatiopaketin
kun kysyjä on sen koonnut. Ja se yhteenveto pitää myös muistaa lähettää.
Mielellään siistittynä, ilman ylimääräisiä otsikkotietoja. Tai sitten lyhyt
toteamus "ei yhteenvetoa" jos vastauksia ei ole tullut. Sitä paitsi useilla
ihmisillä on pelko julkista kirjoittamista kohtaan, mutta postia he kyllä
uskaltavat kirjoittaa. Joten saat luultavasti enemmän vastauksia.

Usein on kohteliasta myös kertoa että ongelma on ratkaistu ja miten se
on ratkaistu. Muuten ihmiset jatkavat vastaamista turhaan.

Ja apua pyytäessäsi ole muutenkin kohtelias. Älä sano "Haluan ohjelman
joka..." Sano "Voisiko joku kertoa mistä saisin..." Iso ero henkilölle joka
miettii vastaako kysymykseesi vai ei.


Artikkeliin viittaaminen

Artikkelien numerot ovat täysin paikallisia. Artikkeli numero 1234 siinä
koneessa jossa sitä luet ei välttämättä ole numero 1234 missään muualla.
Ainoa keino tunnistaa jokin artikkeli yksiselitteisesti on sen
Message-ID:-kenttä.


News.announce.newusers

Tähän ryhmään postataan säännöllisin väliajoin parinkymmenen artikkelin
rypäs jossa on yleisiä ohjeita, ohjeet kansainvälisten uutisryhmien
perustamisesta, lista kaikista uutisryhmistä ja lukuisista
postituslistoista kuvauksineen jne. Ehdottoman suositeltavaa luettavaa.


Eteenpäin maksaminen

Jos joku opettaa sinua, et välttämättä pysty korvaamaan sitä hänelle.
Mutta voi opettaa edelleenkin jotakuta muuta. Samoin newsseissä.  Joissakin
ryhmissä olet kyselijä tai vain lukija, joissakin ryhmissä pystyt itse
osallistumaan keskusteluun ja tarjoamaan jotain muillekin.


Kirjoittajat ja lukijat

Newsseihin kirjoittaminen on esittävä taidemuoto. Muista se. Lukijoiden
määrä on kymmen- tai satakertainen verrattuna kirjoittajiin. Kun vastaat
jonkun kysymykseen kymmenet tai sadat ihmiset joista et ole koskaan
kuullutkaan lukevat sen. Kun käyt flame-sotaa, suuri joukko ihmisiä
arvostelee molempia kirjoittajia.


FAQ

Usein samat kysymykset toistuvat säännöllisin väliajoin. Tällöin usein joku
aktiivikäyttäjä luo kyseiselle ryhmälle Frequently Asked Questions -listan,
joka on yksinkertaisesti lista usein kysytyistä kysymyksistä ja niiden
vastauksista. Lista on sitten tapana postata kerran kuussa tai kahdessa.


vakiojutut

1=0 -todistukset perustuvat yleensä nollalla jakamiseen tai negatiivisen
neliöjuuren väärinkäyttöön.

Kohtisuoraan toisiaan vasten asetetut gyroskoopit _eivät_ kumoa
painovoimaa.

Ohjelmia eri koneille voi hakea anonyymillä ftp:llä koneesta nic.funet.fi.
Sinne arkistoidaan myös joitakin uutisryhmiä, lähinnä ohjelmia.

Se kuolemaa tekevä lapsi joka haluaa postikortteja päästäkseen
ennätyskirjaan on saannut suunnattoman määrän kortteja eikä halua enää
yhtään lisää. 

Modeemivero USA:ssa on kymmenen vuotta vanha juttu, eikä ole tulossa
voimaan. 

Ketjukirjeet ovat kiellettyjä. Älä yritäkään.


----------------------------------------------------------------------


*		Mitä tahansa (wildcard, jokerimerkki), tapa
		merkitä vahvistettua tekstiä

_		Alleviivauksen alku- ja loppumerkki

anonyymi ftp	Palvelu, jossa käyttäjä voi hakea tiedostoja
		palvelimesta, lue opas "Tiedostojen siirto
		FTP:llä"

artikkeli	Yksittäinen juttu joka on postattu newsseihin

ASCII-grafiikka	Kirjaimista ja muista ASCII-merkeistä koottu kuva

BTW		Muuten (By The Way)

crosspostaus	Saman artikkelin lähettäminen useaan ryhmään
		kerralla

fleimi		Vastine artikkelille, yleensä kärkevä, tulinen

followup	Fleimi joka ei polta, asiallinen vastine
		artikkelille

Followup-to:	Kenttä johon kirjoitetaan uutisryhmän nimi johon
		vastaukset tähän artikkeliin halutaan.

FYI		Tiedoksesi (For Your Information)

hymiö		:-) -käännä päätäsi 90 astetta, niin näet
		hymyilevät kasvot

IMHO		Mielestäni (In My Humble Opinion)

Message-ID:	Kenttä jos sisältää uniikin tunnistimen
		kyseiselle news-artikkelille

moderointi	Toimittaminen, ryhmään tulevien artikkelien
		karsiminen

Newsgroups:	Kenttä johon kirjoitetaan kaikki ne uutisryhmät
		joihin tämän artikkelin halutaan menevän

newssit		(Kansainvälinen) keskustelujärjestelmä

postata		Lähettää news-artikkeli

regexp		Säännöllinen lauseke, sci.* tarkoittaa kaikkia
		sci-hierarkian alla olevia ryhmiä

RTFM		Lue opas (Read The Fine Manual)

Rot-13		Tapa koodata teksti siten että käyttäjän on
		tehtävä jotain sen lukemiseksi

ryhmä		Tiettyyn aiheeseen suuntautunut keskusteluryhmä

UNIX		Käyttöjärjestelmä



 ( 143557 ) 

